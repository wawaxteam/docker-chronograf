# Chronograf #

## Run

```
$> docker-compose up -d
```

## Configuration

| Environment Variable    | Description                                               
|-------------------------|------------------------------------------------------
| `INFLUXDB_PROTO`        | InfluxDB logging protocol (default `http`)
| `INFLUXDB_HOST`         | InfluxDB host (default `localhost`)
| `INFLUXDB_PORT`         | InfluxDB port (default `8086`)


## Chronograf ##

More information about [InfluxDB Chronograf](https://influxdb.com/chronograf/index.html)

![Chronograf Screenshot](https://influxdb.com/img/blog/chronograf.gif)