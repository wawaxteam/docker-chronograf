FROM ubuntu:trusty
MAINTAINER Antoine SAUVAGE <antowan@wawax.co>

ENV CHRONOGRAF_VERSION=0.12.0
ENV CHRONOGRAF_BIND=0.0.0.0:10000

RUN apt-get update && apt-get install -y curl && apt-get clean

RUN curl -s -o /tmp/chronograf_latest_amd64.deb https://dl.influxdata.com/chronograf/releases/chronograf_${CHRONOGRAF_VERSION}_amd64.deb && \
  dpkg -i /tmp/chronograf_latest_amd64.deb && \
  rm /tmp/chronograf_latest_amd64.deb && \
  rm -rf /var/lib/apt/lists/*

# Allow Chronograf to accept connections from other hosts
EXPOSE 10000

CMD [ "/opt/chronograf/chronograf" ]
